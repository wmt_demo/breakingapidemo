import { StyleSheet } from 'react-native';
import { ThemeUtils, Color } from 'src/component';

const ImageAspectRatio = 1 / 1.5;
const ImageWidthRatio = 40;

export const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 10,
        paddingTop: 20,
        paddingHorizontal: 20,
    },

    profileImageContainer: {
        width: ThemeUtils.relativeWidth(ImageWidthRatio),
        aspectRatio: ImageAspectRatio,
        backgroundColor: Color.PHOTO_BG,
        borderRadius: 10,
        overflow: 'hidden',
        borderWidth: 3,
        borderColor: Color.BORDER_BG,
    },

    profileImageDummyContainer: {
        ...StyleSheet.absoluteFillObject,
        width: ThemeUtils.relativeWidth(ImageWidthRatio),
        aspectRatio: ImageAspectRatio,
        backgroundColor: Color.PHOTO_BG,
        borderRadius: 10,
        overflow: 'hidden',
        borderWidth: 1,
        borderColor: Color.BORDER_BG,
        start: ThemeUtils.relativeWidth(2),
        transform: [{ rotateZ: '1.5deg' }],
    },

    profileImage: {
        // flex: 1,
        height: ThemeUtils.relativeWidth(ImageWidthRatio) / ImageAspectRatio,
        aspectRatio: ImageAspectRatio,
    },

    nameEffect: {
        textShadowColor: 'rgba(0, 0, 0, 0.3)',
        textShadowOffset: { width: 2, height: 0 },
        textShadowRadius: 5,
    },

    masterNameContainer: {
        backgroundColor: Color.PRIMARY_BACKGROUND,
        padding: 10,
        borderTopEndRadius: 10,
        borderTopStartRadius: 10,
        marginBottom: 10,
        overflow: 'visible'
    },

    masterCharOccupationContainer: {
        backgroundColor: Color.PRIMARY_BACKGROUND,
        padding: 10,
        paddingTop: 10,
        paddingBottom: 5,
        overflow: 'visible',
        transform: [{ rotateZ: '-0.5deg' }],
    },

    masterSeasonContainer: {
        backgroundColor: Color.PRIMARY_BACKGROUND,
        marginTop: 10,
        padding: 10,
        paddingVertical: 20,
        borderBottomEndRadius: 10,
        borderBottomStartRadius: 10,
        overflow: 'visible',
        transform: [{ rotateZ: '-1.5deg' }],
    },
    charDetailsContainer: {
        zIndex: 10,
        marginTop: 20,
        paddingStart: 10,
        paddingEnd: 20,
        overflow: 'visible'
    },
});
