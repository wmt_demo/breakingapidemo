/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { StatusBar, View } from 'react-native';
import { MenuProvider } from 'react-native-popup-menu';

import {
    PersistGate,
    Provider,
    persistor,
    SafeAreaView,
    store,
    Toast,
    SafeAreaProvider,
    CommonStyle,
    NavigationContainer,
    Color,
} from "src/component";

import RootNavigator from 'src/router';

const App: () => React$Node = () => {
    return (
        <Provider store={store}>
            <PersistGate
                loading={null}
                persistor={persistor}>
                <SafeAreaProvider>
                    <StatusBar
                        barStyle={"default"}
                    />
                    <NavigationContainer>
                        <MenuProvider>
                            <RootNavigator />
                        </MenuProvider>
                    </NavigationContainer>
                </SafeAreaProvider>
                {/* <Toast ref={(ref) => Toast.setRef(ref)} /> */}
            </PersistGate>
        </Provider>

    );
};

export default App;
