
import React,
{
    useState,
    useEffect,
    useRef
} from 'react';

import {
    View,
    Image,
    StyleSheet,
    ScrollView
} from 'react-native';

import {
    Label,
    CommonStyle,
    lodash,
    profilePlaceholder,
    FadeImage,
    ThemeUtils,
    EntypoIcon,
    MaterialCommunityIcon,
    Color
} from 'src/component';

import { styles } from './styles';

const BreakingCharDetails = (props) => {

    /*  Init vars   */

    const {
        route
    } = props;

    const breakingCharObj = route.params.personObj;

    /*  Life-cycles Methods */

    useEffect(() => {
        console.log("breakingCharObj: ", breakingCharObj)
    }, []);

    /*  Public Interface Methods */

    /*  Validation Methods  */

    /*  UI Events Methods   */

    const renderProfilePhoto = () => {
        return (
            <>
                <View
                    style={styles.profileImageDummyContainer}>
                    <Image
                        style={styles.profileImage}
                        source={profilePlaceholder}
                        defaultSource={profilePlaceholder}
                        resizeMode={'cover'}
                    />
                </View>
                <View
                    style={styles.profileImageContainer}>
                    {!lodash.isNil(breakingCharObj?.img) &&
                        breakingCharObj?.img !== '' ?
                        <FadeImage
                            style={styles.profileImage}
                            source={{
                                uri: breakingCharObj?.img,
                                // cache: 'reload'
                            }}
                            // defaultSource={profilePlaceholder}
                            resizeMode={'cover'}
                        /> :
                        <Image
                            style={styles.profileImage}
                            source={profilePlaceholder}
                            defaultSource={profilePlaceholder}
                            resizeMode={'cover'}
                        />}
                </View>
            </>
        )
    }

    const renderNameDetails = () => {
        return (
            <View
                style={styles.masterNameContainer}>
                <Label
                    bold
                    align={'left'}
                    mb={10}
                    style={styles.nameEffect}>
                    {`${breakingCharObj?.name ?? 'Unknown'}`}
                    {/* {'skljdfhjksfhjksdhfjk skjdfhjksdhfjkh skjdfhjks hjksdfhjhsjd hjskfhskjdf'} */}
                </Label>
                <Label
                    small
                    mb={10}
                    align={'left'}
                    style={styles.nameEffect}>
                    {`Nick name ▶︎ ${breakingCharObj?.nickname ?? 'Unknown'}`}
                    {/* {'skljdfhjksfhjksdhfjk skjdfhjksdhfjkh skjdfhjks hjksdfhjhsjd hjskfhskjdf'} */}
                </Label>
                <Label
                    align={'left'}
                    small
                    style={styles.nameEffect}>
                    {`Status ▶︎ ${breakingCharObj?.status ?? 'Unknown'}`}
                    {/* {'skljdfhjksfhjksdhfjk skjdfhjksdhfjkh skjdfhjks hjksdfhjhsjd hjskfhskjdf'} */}
                </Label>
                <View style={{
                    position: 'absolute',
                    top: -5,
                    end: -3
                }}>
                    <EntypoIcon
                        name={'pin'}
                        size={ThemeUtils.fontLarge}
                        color={Color.TEXT_PRIMARY} />
                </View>
            </View>
        )
    }

    const renderCharOccupation = (item, index) => {
        return (
            <Label
                key={`${index}`}
                small
                light
                align={'left'}
                // numberOfLines={2}
                mb={10}
                style={styles.nameEffect}>
                {`${item}`}
                {/* {'skljdfhjksfhjksdhfjk skjdfhjksdhfjkh skjdfhjks hjksdfhjhsjd hjskfhskjdf'} */}
            </Label>
        )
    }

    const renderCharOccupationStack = () => {
        return (
            <View style={styles.masterCharOccupationContainer}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    alignContent: 'center',
                    marginBottom: 10
                }}>
                    <Label
                        small
                        bold
                        align={'left'}
                        me={5}
                        style={styles.nameEffect}>
                        {`Occupation`}
                    </Label>
                    <View
                        style={{
                            marginTop: 8
                        }}>
                        <MaterialCommunityIcon
                            name={'arrow-down-right'}
                            size={ThemeUtils.fontNormal}
                            color={Color.TEXT_PRIMARY} />
                    </View>
                </View>

                {(breakingCharObj?.occupation ?? []).map(renderCharOccupation)}
                <View style={{
                    position: 'absolute',
                    top: -3,
                    end: -3,
                }}>
                    <EntypoIcon
                        name={'pin'}
                        size={ThemeUtils.fontLarge}
                        color={Color.TEXT_PRIMARY} />
                </View>
            </View>
        )
    }

    const renderSeasonAppearance = () => {
        return (
            <View style={styles.masterSeasonContainer}>
                <Label
                    align={'left'}
                    small
                    numberOfLines={2}
                    style={styles.nameEffect}>
                    {`Season appearance ▶︎ ${breakingCharObj?.appearance.join(", ")}`}
                    {/* {'skljdfhjksfhjksdhfjk skjdfhjksdhfjkh skjdfhjks hjksdfhjhsjd hjskfhskjdf'} */}
                </Label>
                <View style={{
                    position: 'absolute',
                    top: -3,
                    end: -3,
                }}>
                    <EntypoIcon
                        name={'pin'}
                        size={ThemeUtils.fontLarge}
                        color={Color.TEXT_PRIMARY} />
                </View>
            </View>
        )
    }

    const renderCharDetails = () => {
        return (
            <ScrollView
                style={styles.charDetailsContainer}
                contentContainerStyle={{
                    paddingTop: 0,
                    paddingBottom: 5
                }}>
                <View
                    style={{
                        // transform: [{ rotateZ: '-2deg' }],
                        // paddingTop: 20
                    }}>
                    {renderNameDetails()}
                    {(breakingCharObj?.occupation ?? []).length > 0 &&
                        renderCharOccupationStack()}
                    {(breakingCharObj?.appearance ?? []).length > 0 &&
                        renderSeasonAppearance()}
                </View>
            </ScrollView >
        )
    }

    /*  Custom-Component sub-render Methods */

    return (
        <View style={CommonStyle.master_full_flex}>
            <View style={styles.container}>
                <View
                    style={{
                        transform: [{ rotateZ: '-2deg' }],
                        flex: 1,
                    }}>
                    <View
                        style={{
                            paddingStart: ThemeUtils.relativeWidth(5),
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                            zIndex: 11,
                        }}>
                        {renderProfilePhoto()}
                    </View>
                    {renderCharDetails()}
                </View>
            </View>
        </View>
    );
};

export default BreakingCharDetails;
