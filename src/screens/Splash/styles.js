import {StyleSheet} from 'react-native';
import {Color} from 'src/component';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Color.MASTER_BG,
    },
    logo: {
        width: '40%',
        height: '30%',
    },
});
