import { StyleSheet } from 'react-native';
import { ThemeUtils, Color } from 'src/component';

const ImageAspectRatio = 1 / 1.5;
const ImageWidthRatio = 24;

export default StyleSheet.create({
    container: {
        flex: 1
    },

    headerIconContainer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        // backgroundColor: '#00f',
    },
    headerContainer: {
        paddingTop: 12,
        backgroundColor: Color.MASTER_BG,
        flexDirection: 'row',
        // marginTop: IS_IOS ? 20 : 0,
        // backgroundColor: '#00f3',
        alignItems: 'center',
        justifyContent: 'center',
        paddingStart: 20,
        paddingEnd: 10
    },
    headerOperationContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        // backgroundColor: '#00f3'
    },
    searchTextfieldContainer: {
        flex: 1,
        paddingEnd: 0,
        justifyContent: 'center',
    },

    checkedOption: {
        // backgroundColor: '#00f3', 
        flexDirection: 'row',
        alignItems: 'center'
    },

    flatList: {
        flex: 1
    },

    peopleItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 12,
        // backgroundColor: '#00f4'
    },

    peopleImageNameContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        flex: 1,
        paddingEnd: 15,
        // backgroundColor: '#0ff4',
    },

    profileImageContainer: {
        width: ThemeUtils.relativeWidth(ImageWidthRatio),
        aspectRatio: ImageAspectRatio,
        borderRadius: 10,
        overflow: 'hidden',
        borderWidth: 3,
        borderColor: Color.BORDER_BG,
        backgroundColor: Color.PHOTO_BG,
        transform: [{ rotateZ: '-2deg' }],
    },

    profileImage: {
        // flex: 1,
        height: ThemeUtils.relativeWidth(ImageWidthRatio) / ImageAspectRatio,
        aspectRatio: ImageAspectRatio,
    },

    peopleNameContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flexShrink: 1,
        transform: [{ rotateZ: '-2deg' }]
        // backgroundColor: '#f0f4'
    },

    peopleActionContainer: {
        flexShrink: 1,
        paddingVertical: 10,
        // backgroundColor: '#ff4'
    },
});
