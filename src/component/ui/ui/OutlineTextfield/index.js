import React, {
    useState,
    useEffect,
    createRef
} from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';

import {
    TextField,
    FilledTextField,
    OutlinedTextField
} from 'src/component/ui/vendor/rn-material-ui-textfield';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { Color } from 'src/component/utils/Color';
import ThemeUtils, {
    FontStyle
} from 'src/component/utils/ThemeUtils';

import PropTypes from 'prop-types';
import lodash from 'lodash';

import styles from './styles';

import Label from 'src/component/ui/ui/Label';

import {
    Icon
} from 'src/component/ui';

export default function OutlineTextfield(props) {

    let {
        filled,
        label,
        outlinedLabel,
        outlined,
        secureTextEntry,
        leftIconName = null,
        rightIconName = null,
        maxCharLimit = null,
        error,
        showClearButton,
        onPressClear,
        value,
        makeAutoFocus,
        onChangeText,
        getTextfieldRef,
        renderLeftIconAccessory = () => null,
        renderRightIconAccessory = () => null,
    } = props;

    /*  Life-cycles Methods */

    const [passwordVisible, setPasswordVisible] = useState(secureTextEntry);
    const [textfiledBorderColor, setTextfiledBorderColor] = useState(Color.BORDER_BG);
    // let [textFieldRef] = useState(createRef());

    let textFieldRef = createRef();

    /*  Public Interface Methods */

    const isNull = (text) => {
        return !(lodash.isString(text) &&
            text.trim().length > 0);
    }

    const isCharacterExceed = (string) => {
        let isExceed = false;

        if (maxCharLimit &&
            (typeof string) === 'string') {
            isExceed = string.length > maxCharLimit;
        }

        return isExceed;
    }

    /*  Validation Methods  */



    /*  UI Events Methods   */

    const onChange = (text) => {

        if (maxCharLimit &&
            isCharacterExceed(text)) {
            text = text.substring(0, text.length - 1);
        }

        onChangeText &&
            onChangeText(text);
    }

    const onBlur = () => {
        setTextfiledBorderColor(Color.BORDER_BG);
        props.onBlur &&
            props.onBlur();
    }

    const onFocus = () => {
        setTextfiledBorderColor(Color.SEPTENARY_BG);
        props.onFocus &&
            props.onFocus();
    }

    /*  Custom-Component sub-render Methods */

    return (
        <View>
            {label ? (
                <Label
                    font_medium
                    small
                    mb={15}
                    color={Color.TEXT_QUATERNARY}>
                    {label}
                </Label>
            ) : null}
            <FilledTextField
                inputRef={textFieldRef}
                labelOffset={{
                    y0: 1,
                }}
                inputContainerStyle={[
                    styles.outlinedLabelStyle,
                    {
                        borderColor: textfiledBorderColor,
                        // height: 48
                    }
                ]}
                containerStyle={{
                    // height: 48,
                }}
                contentInset={{
                    label: 0,
                    top: 3,
                    input: 3,
                }}
                masterContainerStyle={{
                    // alignItems: 'center'
                }}
                renderLeftAccessory={renderLeftIconAccessory}
                renderRightAccessory={() => {
                    if (showClearButton) {
                        return (
                            showClearButton &&
                            !isNull(value) &&
                            <TouchableOpacity
                                style={styles.closeIcon}
                                activeOpacity={0.8}
                                onPress={onPressClear}>
                                <MaterialIcon
                                    color={Color.TEXT_PRIMARY}
                                    name={'close'}
                                    size={ThemeUtils.fontLarge}
                                />
                            </TouchableOpacity>
                        );
                    } else if (rightIconName) {
                        return (
                            <View style={styles.downIcon}>
                                {renderRightIconAccessory}
                            </View>
                        );
                    } else {
                        return;
                    }
                }}
                {...props}
                onBlur={() => {
                    onBlur();
                }}
                onFocus={() => {
                    onFocus();
                }}
                error={props.error}
                activeLineWidth={0}
                labelTextStyle={{ fontFamily: FontStyle.medium }}
                titleTextStyle={{ fontFamily: FontStyle.medium }}
                affixTextStyle={{ fontFamily: FontStyle.medium }}
                titleFontSize={ThemeUtils.fontSmall}
                fontSize={ThemeUtils.fontNormal}
                labelFontSize={ThemeUtils.fontSmall}
                baseColor={'transparent'}
                onChangeText={onChange}
                tintColor={'transparent'}
                placeholderTextColor={Color.TEXT_QUATERNARY}
                selectionColor={Color.TEXT_PRIMARY}
                errorLabelColor={Color.TEXT_ERROR}
                errorColor={'transparent'}
                secureTextEntry={passwordVisible}
            />
        </View>
    );
}

OutlineTextfield.propTypes = {
    icEyeColor: PropTypes.string,
    filled: PropTypes.bool,
    outlined: PropTypes.bool,
    outlinedLabel: PropTypes.bool,
    ...TextField.propTypes,
};

OutlineTextfield.defaultProps = {
    icEyeColor: Color.TEXT_PRIMARY,
    filled: false,
    outlined: false,
    outlinedLabel: false,
    ...TextField.defaultProps,
};


