
const Routes = {
    
    /*  Root Stacks    */

    Authenticated: 'Authenticated',
    Splash: 'Splash',

    /*  Non-Authenticated Routes    */

    /*  Authenticated Routes    */

    BreakingCharList: 'BreakingCharList',
    BreakingCharDetails: 'BreakingCharDetails'
};

export default Routes;
