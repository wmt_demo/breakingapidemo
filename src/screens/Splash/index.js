import React, { useEffect } from 'react';

import { View } from 'react-native';

import { styles } from './styles';

import { IS_IOS, Routes, CommonActions, connect } from 'src/component';

const Splash = (props) => {

    //navigate to authenticated route
    const resetToAuth = CommonActions.reset({
        index: 0,
        routes: [{ name: Routes.Authenticated }],
    });

    /*  Life-cycles Methods */

    useEffect(() => {
        let splashDelay = IS_IOS ? 100 : 1000;

        setTimeout(() => {
            props.navigation.dispatch(resetToAuth);
        }, splashDelay);

    }, []);

    /*  Public Interface Methods */

    /*  Validation Methods  */

    /*  UI Events Methods   */

    /*  Custom-Component sub-render Methods */

    return <View style={styles.container} />;
};

//set store values as props
const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Splash);
