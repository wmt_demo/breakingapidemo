import { StyleSheet } from 'react-native';
import { Color } from 'src/component/utils/Color';

export default StyleSheet.create({
    icEye: {
        paddingHorizontal: 0,
        paddingStart: 8,
        paddingEnd: 0,
        // marginTop: 12,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        // backgroundColor: '#00f',
    },
    searchIcon: {
        // height: '100%',
        // paddingBottom: 2,
        paddingHorizontal: 0,
        paddingEnd: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        // backgroundColor: '#00f'
    },
    closeIcon: {
        // paddingBottom: 4,
        paddingHorizontal: 0,
        paddingEnd: 3,
        paddingStart: 8,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        // backgroundColor: '#00f'
    },
    outlinedLabelStyle: {
        borderRadius: 4,
        backgroundColor: Color.WHITE,
        borderWidth: 1,
        borderColor: Color.BORDER_BG,
        // backgroundColor: '#0f03'
    },
    downIcon: {
        // paddingBottom: 5,
        paddingHorizontal: 0,
        paddingEnd: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        // backgroundColor: '#00f'
    },
});
