import React from 'react';
import { Animated, Image, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import lodash from 'lodash';

import {
    IS_IOS,
    IS_ANDROID
} from 'src/component/utils';

class FadeImage extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            defaultImageOpacity: new Animated.Value(1),
            imageOpacity: new Animated.Value(0),
            imageTop: new Animated.Value(100),
        };
        
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        let obj = {};

        return obj;
    }

    onLoadEnd = () => {
        // console.log("onLoadEnd: ")

        const {
            imageOpacity,
            defaultImageOpacity,
            imageTop
        } = this.state;

        const {
            duration = 800,
            onLoad
        } = this.props;

        // return;

        let useNativeDriver = false;

        // Animated.timing(defaultImageOpacity).stop();

        // this.mounted &&
        //     Animated.timing(defaultImageOpacity, {
        //         toValue: 0,
        //         duration: duration,
        //         useNativeDriver: useNativeDriver
        //     }).start(() => {
        //         this.mounted &&
        //             this.setState({
        //                 loaded: true
        //             }, () => {
        //                 this.mounted &&
        //                     Animated.timing(imageOpacity, {
        //                         toValue: 1,
        //                         duration: duration,
        //                         useNativeDriver: useNativeDriver
        //                     }).start(() => {
        //                         if (this.mounted &&
        //                             onLoad) {
        //                             onLoad();
        //                         }
        //                     });
        //             });
        //     });  

        this.mounted &&
            this.setState({
                loaded: true
            }, () => {
                this.mounted &&
                    Animated.timing(imageTop, {
                        toValue: 0,
                        duration: duration,
                        useNativeDriver: useNativeDriver
                    }).start(() => {
                        if (this.mounted &&
                            onLoad) {
                            onLoad();
                        }
                    });
            });
    }

    render() {
        const {
            loaded,
            imageOpacity,
            imageTop,
            defaultImageOpacity,
            imageVersion
        } = this.state;

        const {
            defaultSource,
            style,
            source,
            resizeMode = 'contain'
        } = this.props;

        const {
            placeholderResizeMode = resizeMode
        } = this.props;

        let updatedURL = source?.uri;
        let updatedSource = {
            ...source
        };

        return (
            <View style={[style]}>
                {!loaded &&
                    defaultSource &&
                    <Animated.Image
                        source={defaultSource}
                        style={[style, {
                            opacity: defaultImageOpacity
                        }]}
                        resizeMode={placeholderResizeMode} />}
                <View
                    style={{
                        position: 'absolute',
                        top: 0,
                        start: 0
                    }}>
                    <Image
                        source={defaultSource}
                        style={[style]}
                        resizeMode={placeholderResizeMode} />
                </View>
                <Animated.View
                    style={[style, {
                        // opacity: imageOpacity,
                        position: 'absolute',
                        // top: '-50%',
                        top: imageTop.interpolate({
                            inputRange: [0, 100],
                            outputRange: ['0%', '-100%'],
                        }),
                        start: 0,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }]}>
                    {IS_IOS ?
                        <FastImage
                            source={updatedSource}
                            style={[style,
                                loaded ? {} : {
                                    width: 0,
                                    height: 0
                                }]}
                            onLoadEnd={this.onLoadEnd.bind(this)}
                            resizeMode={resizeMode}
                        /> :
                        <Image
                            style={[style,
                                loaded ? {} : {
                                    width: 0,
                                    height: 0
                                }]}
                            source={updatedSource}
                            resizeMode={resizeMode}
                            onLoadEnd={this.onLoadEnd.bind(this)} />}
                </Animated.View>
            </View>
        );
    }
}


export default FadeImage;