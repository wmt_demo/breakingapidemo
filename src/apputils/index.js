
/*=======================================
 |   Server Configuration - Start
 *=======================================*/

 const ServerType = {
    PROD: 'prod',
    DEV: 'dev',
    LOCAL: 'local'
}

//-----------------------------------
// - Server Environment Config
//-----------------------------------

const ServerConfig = {

    //----------------------------
    // - Production Server
    //----------------------------

    [ServerType.PROD]: {

        //  V1:
        BASE_URL: '',

    },

    //----------------------------
    // - Development Server
    //----------------------------

    [ServerType.DEV]: {

        //  V1:
        BASE_URL: 'https://breakingbadapi.com',

    },

    //----------------------------
    // - Local Server
    //----------------------------

    [ServerType.LOCAL]: {

        //  V1:
        BASE_URL: '',
    }
}

//-----------------------------------
// - Server Environment Selection
//-----------------------------------

//  Production Server
// const CurrentServerType = ServerType.PROD;

//  Development Server
const CurrentServerType = ServerType.DEV;

//  Local Server
// const CurrentServerType = ServerType.LOCAL;

//-----------------------------------
// - Selected Server Environment URLs
//-----------------------------------

export const BASE_URL = ServerConfig[CurrentServerType].BASE_URL;

export const API_URL = `${BASE_URL}/api`;


/*=======================================
 |   Server Configuration - End
 *=======================================*/

//----------------------------
// - Third party API
//----------------------------


//----------------------------
// - OTHER UTILS
//----------------------------


export default {
    BASE_URL,
    API_URL,
}
