import { StyleSheet } from 'react-native';
import { Color } from './Color';
import ThemeUtils from "./ThemeUtils";
import { IS_IOS } from "./"

const Style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.MASTER_BG,
    },
    content_center: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    full_flex: {
        flex: 1,
        // backgroundColor: '#00f'
    },
    master_full_flex: {
        flex: 1,
        backgroundColor: Color.MASTER_BG,
    },
    full_center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitleStyle: {
        // flex: 1,
        // width: '100%',
        color: Color.TEXT_PRIMARY,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        textAlignVertical: 'center',
        fontFamily: ThemeUtils.FontStyle.regular,
        fontWeight: '700',
        fontSize: IS_IOS ? ThemeUtils.fontLarge : ThemeUtils.fontLarge,
        
    },
    headerLeftTitleStyle: {
        flexShrink: 1,
        color: Color.TEXT_PRIMARY,
        alignSelf: 'center',
        textAlign: 'left',
        justifyContent: 'center',
        textAlignVertical: 'center',
        fontFamily: ThemeUtils.FontStyle.regular,
        fontWeight: '700',
        fontSize: IS_IOS ? ThemeUtils.fontLarge : ThemeUtils.fontLarge,
        // backgroundColor: '#00f3'
    },
    headerCustomTitleStyle: {
        flex: 1,
        width: 100,
        color: Color.TEXT_PRIMARY,
        alignSelf: 'center',
        textAlign: 'left',
        justifyContent: 'center',
        textAlignVertical: 'center',
        fontFamily: ThemeUtils.FontStyle.regular,
        fontWeight: '700',
        fontSize: IS_IOS ? ThemeUtils.fontLarge : ThemeUtils.fontLarge,
        backgroundColor: '#00f3'
    },
    headerTitleContainerStyle: {
        flexShrink: 1,
        // width: '80%',
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#00f3'
    },
    noDataContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    noDataInList: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    fullFlexGrow: {
        flexGrow: 1
    }
});

export default Style;
