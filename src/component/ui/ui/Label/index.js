import React from 'react';
import { Text, Platform } from 'react-native';

import PropTypes from 'prop-types';

import { Color } from 'src/component/utils/Color';
import ThemeUtils from 'src/component/utils/ThemeUtils';

const IS_IOS = Platform.OS === 'ios';

class Label extends React.Component {
    onClick = () => {
        if (this.props.onPress) {
            this.props.onPress();
        }
    };

    render() {
        let stylesArray = [];
        if (this.props.xxxlarge) {
            stylesArray.push({ fontSize: ThemeUtils.fontXXXLarge });
        } else if (this.props.xxlarge) {
            stylesArray.push({ fontSize: ThemeUtils.fontXXLarge });
        } else if (this.props.xlarge) {
            stylesArray.push({ fontSize: ThemeUtils.fontXLarge });
        } else if (this.props.large) {
            stylesArray.push({ fontSize: ThemeUtils.fontLarge });
        } else if (this.props.normal) {
            stylesArray.push({ fontSize: ThemeUtils.fontNormal });
        } else if (this.props.small) {
            stylesArray.push({ fontSize: ThemeUtils.fontSmall });
        } else if (this.props.xsmall) {
            stylesArray.push({ fontSize: ThemeUtils.fontXSmall });
        } else if (this.props.xxsmall) {
            stylesArray.push({ fontSize: ThemeUtils.fontXXSmall });
        } else {
            stylesArray.push({ fontSize: ThemeUtils.fontNormal });
        }


        if (IS_IOS) {
            if (this.props.bold) {
                stylesArray.push({ fontWeight: '600' });
            } else if (this.props.boldest) {
                stylesArray.push({ fontWeight: '700' });
            } else if (this.props.proBold) {
                stylesArray.push({ fontWeight: '800' });
            } else if (this.props.proBoldest) {
                stylesArray.push({ fontWeight: '900' });
            } else if (this.props.bolder) {
                stylesArray.push({ fontWeight: 'bold' });
            } else if (this.props.light) {
                stylesArray.push({ fontWeight: '400' });
            } else if (this.props.semilight) {
                stylesArray.push({ fontWeight: '500' });
            } else if (this.props.lighter) {
                stylesArray.push({ fontWeight: '300' });
            } else if (this.props.lightest) {
                stylesArray.push({ fontWeight: '200' });
            } else {
                stylesArray.push({ fontWeight: 'normal' });
            }

            if (this.props.font_medium) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.medium });
            } else if (this.props.font_bold) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.bold });
            } else {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.regular });
            }

        } else {

            if (this.props.font_medium) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.medium });
            } else if (this.props.font_bold) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.bold });
            } else {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.regular });
            }

            if (this.props.bold) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.semiBold });
            } else if (this.props.boldest) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.bold });
            } else if (this.props.proBold) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.extraBold });
            } else if (this.props.proBoldest) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.black });
            } else if (this.props.bolder) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.bold });
            } else if (this.props.light) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.regular });
            } else if (this.props.semilight) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.semiBold });
            } else if (this.props.lighter) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.light });
            } else if (this.props.lightest) {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.light });
            } else {
                stylesArray.push({ fontFamily: ThemeUtils.FontStyle.regular });
            }
        }



        stylesArray.push({
            color: this.props.color,
            marginTop: this.props.mt,
            marginBottom: this.props.mb,
            marginStart: this.props.ms,
            marginEnd: this.props.me,
            textAlign: this.props.align,
            margin: this.props.margin
        });
        stylesArray.push(this.props.style);
        let numberOfLines = this.props.singleLine
            ? 1
            : this.props.numberOfLines
                ? this.props.numberOfLines
                : null;

        return (
            <Text
                selectable={this.props.selectable} 
                selectionColor={this.props.selectionColor}
                ellipsizeMode={this.props.ellipsizeMode}
                numberOfLines={numberOfLines}
                style={stylesArray}
                onPress={this.props.onPress}>
                {this.props.children}
            </Text>
        );
    }
}

Label.defaultProps = {
    xxsmall: false,
    xsmall: false,
    small: false,
    normal: false,
    large: false,
    xlarge: false,
    xxlarge: false,
    xxxlarge: false,
    bold: false,
    bolder: false,
    lighter: false,
    light: false,
    color: Color.TEXT_PRIMARY,
    font_bold: false,
    font_medium: false,
    font_regular: true,
    align: 'left',
    selectable: false,
    selectionColor: null,
    mt: 0,
    mb: 0,
    ms: 0,
    me: 0,
    margin: 0,
    singleLine: false,
};
Label.propTypes = {
    xsmall: PropTypes.bool,
    small: PropTypes.bool,
    normal: PropTypes.bool,
    large: PropTypes.bool,
    xlarge: PropTypes.bool,
    xxlarge: PropTypes.bool,
    xxxlarge: PropTypes.bool,
    bold: PropTypes.bool,
    bolder: PropTypes.bool,
    light: PropTypes.bool,
    lighter: PropTypes.bool,
    color: PropTypes.string,
    font_bold: PropTypes.bool,
    font_medium: PropTypes.bool,
    font_regular: PropTypes.bool,
    mt: PropTypes.number,
    margin: PropTypes.number,
    mb: PropTypes.number,
    ms: PropTypes.number,
    me: PropTypes.number,
    align: PropTypes.string,
    singleLine: PropTypes.bool,
};
export default Label;
