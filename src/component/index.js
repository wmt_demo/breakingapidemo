
import Routes from 'src/router/Routes';

import { PersistGate } from 'redux-persist/integration/react';
import { Provider, connect } from 'react-redux';
import { persistor, store } from './redux/store';
import Actions from './redux/action';

import {
    useRoute,
    useNavigationState,
    CommonActions,
    DefaultTheme,
    DrawerActions,
    StackActions,
    NavigationContainer
} from '@react-navigation/native';

import {
    createNativeStackNavigator
} from '@react-navigation/native-stack';

import { SafeAreaView, SafeAreaProvider } from 'react-native-safe-area-context';

import lodash from 'lodash';
import NetInfo from '@react-native-community/netinfo';

/*  UI  */

/*  Icons  */

import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/Feather';
import EntypoIcon from 'react-native-vector-icons/Entypo';

/*  Assets  */

import userPlaceholder from "src/component/ui/assets/images/person.png";
import profilePlaceholder from "src/component/ui/assets/images/profile_placeholder.png";

/*  Utils  */

import { Color } from 'src/component/utils/Color';
import Messages from 'src/component/utils/MessageUtils';
import Constants from 'src/component/utils/Constants';

import CommonStyle from 'src/component/utils/CommonStyles';
import ThemeUtils from 'src/component/utils/ThemeUtils';
import Strings from 'src/component/utils/Strings';
import DummyData from 'src/component/utils/DummyData';

import {
    PasswordValidate,
    validation
} from 'src/component/utils/ValidationUtils';

import {
    IS_IOS,
    IS_ANDROID,
    isNetworkConnected,
} from './utils';

/*  UI  */

import FloatingActionButton from 'src/component/ui/ui/FloatingActionButton';
import Hr from 'src/component/ui/ui/Hr';
import Label from 'src/component/ui/ui/Label';
import Ripple from 'src/component/ui/ui/Ripple';
import {
    Toast,
    ToastType,
    showToast
} from 'src/component/ui/ui/Toast';
import FadeImage from 'src/component/ui/ui/FadeImage';
import OutlineTextfield from 'src/component/ui/ui/OutlineTextfield';

/*  View  */

import Dialog from 'src/component/ui/view/Dialog';

/*  Vendor  */


/*  Other   */


export {
    Routes,

    /*  Start of Redux  */

    Provider,
    PersistGate,
    persistor,
    store,
    Actions,
    connect,

    /*  End of Redux  */

    /*  Start of Vendors  */

    useRoute,
    useNavigationState,
    CommonActions,
    DrawerActions,
    StackActions,
    NavigationContainer,
    createNativeStackNavigator,
    SafeAreaView,
    SafeAreaProvider,
    lodash,
    NetInfo,

    /*  Icons  */

    IonIcon,
    MaterialCommunityIcon,
    MaterialIcon,
    AntDesignIcon,
    FontAwesomeIcon,
    FeatherIcon,
    EntypoIcon,

    /*  End of Vendors  */


    /*  Start of Assets  */

    userPlaceholder,
    profilePlaceholder,

    /*  End of Assets  */

    /*  Start of UI */

    /*  UI  */

    FloatingActionButton,
    Hr,
    Label,
    Ripple,
    Toast,
    ToastType,
    showToast,
    FadeImage,
    OutlineTextfield,

    /*  View  */

    Dialog,

    /*  Vendor  */

    /*  End of UI   */

    /*  Start of Utils  */

    IS_IOS,
    IS_ANDROID,
    Color,
    Messages,
    Constants,
    CommonStyle,
    ThemeUtils,
    Strings,
    DummyData,
    isNetworkConnected,
    PasswordValidate,
    validation,
    // NavigationService,
    // AppManager,
    // DummyData,

    /*  End of Utils  */

    /*  Start of Other Constants */

    /*  End of Other Constants */

};
