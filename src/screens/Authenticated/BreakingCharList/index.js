
import React,
{
    useState,
    useEffect,
    useRef
} from 'react';

import {
    View,
    FlatList,
    Image,
    TouchableOpacity
} from 'react-native';

// import {Controller, useForm} from 'react-hook-form';

import {
    Label,
    CommonStyle,
    Constants,
    profilePlaceholder,
    FadeImage,
    ThemeUtils,
    Color,
    MaterialIcon,
    lodash,
    AntDesignIcon,
    MaterialCommunityIcon,
    OutlineTextfield,
    DummyData,
    Routes
} from 'src/component';

import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

import styles from './styles';

import {
    APIURL,
    RequestManager
} from "src/api";

const requestManager = new RequestManager();

const SeasonAppearTypeArray = [0, 1, 2, 3, 4, 5];

const BreakingCharList = (props) => {

    const {
        navigation
    } = props;

    /*  Init vars   */

    const [searchText, setSearchText] = useState('');
    // const [breakingCharArray] = useState(DummyData.BreakingCharArray);
    // const [filteredBreakingCharArray, setFilteredBreakingCharArray] = useState(DummyData.BreakingCharArray);

    const [breakingCharArray, setBreakingCharArray] = useState([]);
    const [filteredBreakingCharArray, setFilteredBreakingCharArray] = useState([]);

    const [selectedSeaAppTypeArray, setSelectedSeaAppTypeArray] = useState([0]);

    /*  Life-cycles Methods */

    useEffect(() => {
        getBreakingCharRequest();
    }, []);


    useEffect(() => {
        filterCharBySearchText();
    }, [searchText, selectedSeaAppTypeArray]);

    /*  Public Interface Methods */

    const filterCharBySearchText = () => {

        let filteredCharArray = breakingCharArray.slice();

        if (lodash.isString(searchText)) {
            let trimmedSearchText = searchText.trim();
            if (trimmedSearchText.length > 0) {

                filteredCharArray = breakingCharArray.filter((item) => {
                    return item?.name?.includes(trimmedSearchText);
                });

            } else {
                filteredCharArray = breakingCharArray.slice();
            }
        } else {
            filteredCharArray = breakingCharArray.slice();
        }

        // filteredCharArray

        if (selectedSeaAppTypeArray.length > 0 &&
            !selectedSeaAppTypeArray.includes(0)) {
            filteredCharArray = filteredCharArray.filter((item) => {
                return selectedSeaAppTypeArray.every((type) => {
                    return (item?.appearance ?? []).includes(type);
                });
            });
        }

        console.log(filteredCharArray);

        setFilteredBreakingCharArray(filteredCharArray);
    }

    /*  Validation Methods  */

    /*  UI Events Methods   */

    const onPressClearSearchText = () => {
        setSearchText('');
    }

    const onChangeText = (text) => {
        setSearchText(text);
    }

    const onSelectSeaAppType = (type) => {

        if (type === 0) {
            setSelectedSeaAppTypeArray([type]);
            return;
        }

        if (selectedSeaAppTypeArray.includes(type)) {
            let seaAppSet = new Set(selectedSeaAppTypeArray);
            seaAppSet.delete(0);
            seaAppSet.delete(type);

            let updatedSelectedSeaAppTypeArray = [...seaAppSet];
            setSelectedSeaAppTypeArray(updatedSelectedSeaAppTypeArray);
        } else {
            let seaAppSet = new Set(selectedSeaAppTypeArray);
            seaAppSet.delete(0);
            seaAppSet.add(type);

            let updatedSelectedSeaAppTypeArray = [...seaAppSet];
            setSelectedSeaAppTypeArray(updatedSelectedSeaAppTypeArray);
        }
    }

    const onPressBreakingCharItem = (item) => {
        let params = {
            personObj: Object.assign({}, item)
        };

        navigation.navigate(Routes.BreakingCharDetails, params)
    }

    const getBreakingCharRequest = () => {

        // setIsLoaderVisible(true);

        let params = {
        };

        requestManager.doRequest(
            APIURL.API_GET_BREAKING_CHARACTER,
            params,
            onResponse,
            onError);
    };

    /*  Server Response Methods  */

    const onResponse = async (response, reqId) => {

        switch (reqId) {
            case APIURL.API_GET_BREAKING_CHARACTER.id: {
                // setIsLoaderVisible(false);

                switch (response.status) {
                    case Constants.ResponseCode.OK:
                    case Constants.ResponseCode.CREATED:
                        {

                            let dataArray = response?.data?.data ?? [];
                            setBreakingCharArray(dataArray);
                            setFilteredBreakingCharArray(dataArray);

                            break;
                        }
                }
                break;
            }

        }
    };

    const onError = async (error, reqId) => {
        // setIsLoaderVisible(false);

        if (!lodash.isNil(error?.message)) {
            // showToast('', error?.message, ToastType.ERROR);
        }

        switch (error.status) {
            case Constants.ResponseCode.UNPROCESSABLE_REQUEST:
            case Constants.ResponseCode.INTERNAL_SERVER_ERROR:
            case Constants.ResponseCode.UNAUTHORIZED:

            case Constants.ResponseCode.NO_INTERNET:
            case Constants.ResponseCode.TOKEN_INVALID:
                {

                    break;
                }
        }

        switch (reqId) {

        }
    };

    /*  Custom-Component sub-render Methods */

    const CheckedOption = (props) => (
        <MenuOption value={props.value} onSelect={props.onSelect}>
            <View style={styles.checkedOption}>
                <MaterialCommunityIcon
                    color={props.checked ?
                        Color.TEXT_MEDIUM :
                        Color.TRANSPARENT}
                    name={'check-bold'}
                    size={ThemeUtils.fontNormal} />
                <Label
                    ms={10}
                    small
                    color={Color.TEXT_MEDIUM}>
                    {props.text === 0 ? 'Any' : props.text}
                </Label>
            </View>
        </MenuOption>
    )

    const renderMenuOptions = () => {
        return SeasonAppearTypeArray.map((item, index) => {
            return (
                <CheckedOption
                    key={`${index}`}
                    optionsContainerStyle={{
                        padding: 0
                    }}
                    onSelect={() => {
                        onSelectSeaAppType(item);
                    }}
                    checked={selectedSeaAppTypeArray.includes(item)}
                    text={item}>
                </CheckedOption>
            )
        })
    }

    const renderMenu = () => {
        return (
            <Menu>
                <MenuTrigger
                    customStyles={{
                        triggerWrapper: { paddingBottom: 0 },
                        TriggerTouchableComponent: TouchableOpacity,
                    }}>
                    <View
                        style={styles.headerOperationContainer}>
                        <View
                            style={styles.headerIconContainer}>
                            <MaterialIcon
                                name={'more-vert'}
                                size={ThemeUtils.fontXLarge}
                                color={Color.TEXT_PRIMARY} />
                        </View>
                    </View>
                </MenuTrigger>
                <MenuOptions customStyles={{
                    optionsContainer: {
                        width: '58%',
                        paddingBottom: 5,
                        borderRadius: 15,
                        alignContent: 'stretch',
                        alignSelf: 'stretch',
                        paddingHorizontal: 10,
                        paddingTop: 10,
                        paddingBottom: 15,
                    },
                    optionWrapper: {
                        paddingStart: 10,
                        paddingVertical: 10,
                        overflow: 'hidden'
                    },
                }}>
                    <View style={{ transform: [{ rotateZ: '-4deg' }], }}>
                        <Label
                            style={{
                                textShadowColor: 'rgba(0, 0, 0, 0.25)',
                                textShadowOffset: { width: 2, height: 0 },
                                textShadowRadius: 5,
                            }}
                            align={'center'}
                            bold
                            small
                            ms={10}
                            me={10}
                            mb={7}
                            mt={10}>{`Filter by Appearance`}</Label>
                        {renderMenuOptions()}
                    </View>
                </MenuOptions>
            </Menu>
        )
    }

    const renderNavigationIconContainer = (props) => {
        return (
            <TouchableOpacity
                style={styles.headerIconContainer}
                activeOpacity={0.85}
                onPress={() => {
                    props.onPress();
                }}>
                <MaterialIcon
                    name={props.iconName}
                    size={ThemeUtils.fontXLarge}
                    color={Color.TEXT_PRIMARY} />
            </TouchableOpacity>
        )
    }


    const renderLeftIconAccessory = () => {
        return (
            <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100%',
                    paddingEnd: 8
                }}>
                <AntDesignIcon
                    name={'search1'}
                    size={ThemeUtils.fontLarge}
                    color={Color.TEXT_PRIMARY} />
            </View>
        )
    }


    const renderNavigationHeaderContainer = () => {
        return (
            <View
                style={styles.headerContainer}>
                <View style={styles.searchTextfieldContainer}>
                    <OutlineTextfield
                        // makeAutoFocus={true}
                        leftIconName={'glass'}
                        renderLeftIconAccessory={renderLeftIconAccessory}
                        showClearButton={true}
                        placeholder={'Search'}
                        value={searchText}
                        onChangeText={(text) => onChangeText(text)}
                        onPressClear={onPressClearSearchText}
                    />
                </View>
                {renderMenu()}
            </View>
        )
    }


    const renderBreakingCharItem = (item) => {
        return (
            <TouchableOpacity
                style={styles.peopleItemContainer}
                activeOpacity={0.95}
                onPress={() => {
                    onPressBreakingCharItem(item);
                }}>
                <View
                    style={styles.peopleImageNameContainer}>
                    <View
                        style={styles.profileImageContainer}>
                        {!lodash.isNil(item?.img) &&
                            item?.img !== '' ?
                            <FadeImage
                                style={styles.profileImage}
                                source={{
                                    uri: item?.img,
                                    // cache: 'reload'
                                }}
                                // defaultSource={profilePlaceholder}
                                resizeMode={'cover'}
                            /> :
                            <Image
                                style={styles.profileImage}
                                source={profilePlaceholder}
                                defaultSource={profilePlaceholder}
                                resizeMode={'cover'}
                            />}
                    </View>
                    <View
                        style={styles.peopleNameContainer}>
                        <Label
                            normal
                            bold
                            align={'left'}
                            numberOfLines={2}
                            ms={11}
                            style={{
                                textShadowColor: 'rgba(0, 0, 0, 0.3)',
                                textShadowOffset: { width: 2, height: 0 },
                                textShadowRadius: 5,
                            }}>
                            {`${item?.name ?? 'Unknown'}`}
                            {/* {'skljdfhjksfhjksdhfjk skjdfhjksdhfjkh skjdfhjks hjksdfhjhsjd hjskfhskjdf'} */}
                        </Label>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    const renderListContainer = () => {
        return (
            <FlatList
                style={styles.flatList}
                // contentContainerStyle={peopleArray.length > 0 ?
                //     styles.contentContainer : CommonStyle.fullFlexGrow}
                data={filteredBreakingCharArray}
                keyExtractor={(item, index) => `${index}`}
                renderItem={({ item }) => renderBreakingCharItem(item)}
            // ListHeaderComponent={renderNoResultFoundContainer}
            // ListHeaderComponentStyle={peopleArray.length > 0 ?
            //     {} : CommonStyle.noDataInList}
            // refreshControl={
            //     <RefreshControl
            //         refreshing={isRefreshing}
            //         onRefresh={() => {
            //             setIsRefreshing(true);
            //             getBlockedPeopleRequest();
            //         }}
            //     />
            // }
            />
        )
    }

    return (
        <View style={CommonStyle.master_full_flex}>
            <View style={styles.container}>
                {renderNavigationHeaderContainer()}
                {renderListContainer()}
            </View>
        </View>
    );

};

export default BreakingCharList;
