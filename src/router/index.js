
import React from 'react';

import {
    createNativeStackNavigator,
    Routes,
} from 'src/component';

import SplashScreen from 'src/screens/Splash';
import Authenticated from './Authenticated';

const Stack = createNativeStackNavigator();

const navigator = () => {
    return (
        <Stack.Navigator initialRouteName={Routes.Splash}
            screenOptions={({ navigation }) => ({
                headerTitleAlign: 'center',
                headerShown: false
            })}>
            <Stack.Screen name={Routes.Splash} component={SplashScreen} />
            <Stack.Screen
                header={null}
                name={Routes.Authenticated}
                component={Authenticated}
            />
        </Stack.Navigator>
    );
};

export default navigator;
