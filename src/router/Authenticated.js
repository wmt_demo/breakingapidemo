
import React from 'react';
import {
    View,
    StyleSheet
} from 'react-native';

import {
    createNativeStackNavigator,
    CommonStyle,
    Routes,
    Ripple,
    ThemeUtils,
    Color,
    MaterialIcon
} from 'src/component';


// Screens Name

import BreakingCharList from 'src/screens/Authenticated/BreakingCharList';
import BreakingCharDetails from 'src/screens/Authenticated/BreakingCharDetails';

const styles = StyleSheet.create({
    headerStyle: {
        elevation: 0,
        borderBottomWidth: 0,
        shadowOpacity: 0,
        shadowRadius: 0,
        shadowOffset: {
            height: 0,
        },
        shadowColor: Color.TRANSPARENT,
        backgroundColor: Color.MASTER_BG,
    },
    headerStyleWithBorder: {
        elevation: 0,
        borderBottomWidth: 1,
        shadowOpacity: 0,
        borderBottomColor: Color.BORDER_BG,
        backgroundColor: Color.MASTER_BG,
    },
});

const HeaderLeftBack = (navigation, iconColor = Color.TEXT_PRIMARY) => {
    return (
        <View style={styles.headerLeftCont}>
            <Ripple style={styles.icHeaderLeft} onPress={() => navigation.goBack()}>
                <MaterialIcon
                    name={'arrow-back-ios'}
                    size={ThemeUtils.fontLarge}
                    color={iconColor}
                />
            </Ripple>
        </View>
    );
};

const Stack = createNativeStackNavigator();

const AuthNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={__DEV__ ? Routes.BreakingCharList : Routes.BreakingCharList}
            screenOptions={({ navigation }) => ({
                headerTitleAlign: 'center',
                headerLeft: () => HeaderLeftBack(navigation),
                headerTitleAllowFontScaling: false,
                headerShown: true,
                headerStyle: styles.headerStyle,
                headerTintColor: Color.TEXT_PRIMARY,
                headerTitleStyle: CommonStyle.headerTitleStyle,
                headerTitleContainerStyle: CommonStyle.headerTitleContainerStyle,
            })}>
            <Stack.Screen
                name={Routes.BreakingCharList}
                component={BreakingCharList}
                options={{
                    headerLeft:null,
                    headerTitle: 'Breaking Characters'
                }}
            />
            <Stack.Screen
                name={Routes.BreakingCharDetails}
                component={BreakingCharDetails}
                options={{
                    headerTitle: 'Profile'
                }}
            />
        </Stack.Navigator>
    );
};

export default AuthNavigator;
